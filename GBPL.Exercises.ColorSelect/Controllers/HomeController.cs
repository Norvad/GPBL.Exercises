﻿using GBPL.Exercises.ColorSelect.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GBPL.Exercises.ColorSelect.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(new ColorInfo() {  ActionResultText = "this is index " + DateTime.Now.ToString()});
        }

        [HttpPost]
        public ActionResult Index([Bind(Include = "Name")]ColorInfo colorInfo)
        {
            var result = new ColorInfo();
            if (ModelState.IsValid)
            {
                try
                {
                    var db = new Data.ColorSelectDataContext();
                    db.Colors.InsertOnSubmit(new Data.Color() { Name = colorInfo.Name });
                    db.SubmitChanges(System.Data.Linq.ConflictMode.FailOnFirstConflict);

                    result.ActionResultText = "Цвет " + colorInfo.Name + " зарегистрирован";
                }
                catch
                {
                    result.ActionResultText = "Такой цвет уже есть";
                }
            }
            return View(result);
        }
    }
}