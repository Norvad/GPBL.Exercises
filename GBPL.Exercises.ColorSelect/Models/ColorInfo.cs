﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace GBPL.Exercises.ColorSelect.Models
{
    public class ColorInfo
    {
        [Required]
        [StringLength(50, ErrorMessage = "Слишком заковыристо")]
        [Column("Name")]
        [Display(Name = "Цвет")]
        public string Name { get; set; }

        /// <summary>
        /// сообщение
        /// </summary>
        public string ActionResultText { get; set; }
    }
}