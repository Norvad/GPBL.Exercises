﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace GBPL.Exercises.WordCounter.Util
{
    public class ResourceUtil
    {
        public static Stream GetEmbeddedStream(string fileName)
        {
            var assembly = Assembly.GetCallingAssembly();
            var fullFileName = (from n in assembly.GetManifestResourceNames()
                                where n.EndsWith("Embedded." + fileName)
                                select n).FirstOrDefault();

            return assembly.GetManifestResourceStream(fullFileName);
        }
    }
}
