﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GBPL.Exercises.WordCounter.Model
{
    public class WordInfo
    {
        public string Word = "";
        public int Occurances = 0;
    }
}
