﻿using GBPL.Exercises.WordCounter.Model;
using GBPL.Exercises.WordCounter.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GBPL.Exercises.WordCounter
{
    [TestClass]
    public class Start
    {
        protected Regex regexWord = new Regex(@"[A-Za-zА-Яа-яё-]+", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        protected string OutputFileName = "wordCounter.txt";

        [TestMethod]
        public void MainTest() 
        {
            var vocabulary = ReadVocabulary();
            var location = WriteVocabulary(vocabulary.OrderByDescending(i => i.Occurances));

            Process.Start(location);
        }

        protected WordInfo[] ReadVocabulary() 
        {
            var vocabulary = new Dictionary<string, WordInfo>();
            WordInfo wordInfo;

            using (var stream = ResourceUtil.GetEmbeddedStream("Sample.txt"))
            {
                using (var reader = new StreamReader(stream, Encoding.GetEncoding(1251)))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine();
                        var words = regexWord
                            .Matches(line)
                            .Cast<Match>()
                            .Where(i => !string.IsNullOrWhiteSpace(i.Value))
                            .Select(i => i.Value.Trim().ToLower())
                            .ToArray();

                        foreach (var word in words)
                        {
                            if (vocabulary.TryGetValue(word, out wordInfo))
                                wordInfo.Occurances += 1;
                            else
                                vocabulary.Add(word, new WordInfo() { Word = word, Occurances = 1 });
                        }
                    }
                }
            }

            return vocabulary.Values.ToArray();
        }

        protected string WriteVocabulary(IEnumerable<WordInfo> vocabulary) 
        {
            var filePath = Path.Combine(Path.GetTempPath(), OutputFileName);

            if (File.Exists(filePath))
                File.Delete(filePath);
            
            using (var file = File.AppendText(filePath))
                foreach (var wordInfo in vocabulary)
                    file.WriteLine(wordInfo.Word + "\t" + wordInfo.Occurances);

            return filePath;
        }
    }
}
